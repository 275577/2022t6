GRUPPO NUMERO 6
L'obiettivo del progetto è quello di monitorare il traffico di un web server e di seganalare gli eventuali errori.

RUOLI : 
Umberto Pollarini : Programmatore Frontend, Tester, Programmatore Backend, GUI Expert
Danilo Bellocchio : Analyst, Programmatore Backend, GUI Expert
Gianmaria Alessi : Programmatore Frontend, Tester, GUI Expert
Riccardo Forni : Tester, Analyst, System Engineer, Programmatore Backend
Massimiliano Nardone : Programmatore Backend, Jack of all trades
Anas Raouf :  Programmatore Frontend, Tester, Programmatore Backend 
